package com.youlfey.web.driver.engine.core.execution;

import com.youlfey.web.driver.engine.core.annotation.InjectContextManager;
import com.youlfey.web.driver.engine.core.annotation.InjectProcessStorage;
import com.youlfey.web.driver.engine.core.annotation.InjectWebDriver;
import com.youlfey.web.driver.engine.core.annotation.InjectWindowsHandler;
import com.youlfey.web.driver.engine.core.context.ContextManager;
import com.youlfey.web.driver.engine.core.context.SimpleContextManagerInMemory;
import com.youlfey.web.driver.engine.core.drivers.WindowsHandler;
import com.youlfey.web.driver.engine.core.enums.FeedbackStatus;
import com.youlfey.web.driver.engine.core.exception.ScriptExecutionException;
import com.youlfey.web.driver.engine.core.groovy.GroovyExecutor;
import com.youlfey.web.driver.engine.core.groovy.runners.GroovyRunnerInstance;
import com.youlfey.web.driver.engine.core.pojo.ExecutableContext;
import com.youlfey.web.driver.engine.core.pojo.ExecutionContainer;
import com.youlfey.web.driver.engine.core.pojo.WebDriverProcessFeedback;
import com.youlfey.web.driver.engine.core.services.CompletedProcessStorage;
import com.youlfey.web.driver.engine.core.services.impl.LibrariesContainer;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;

import java.util.Arrays;
import java.util.Set;
import java.util.function.Predicate;

import static com.youlfey.web.driver.engine.core.execution.GroovyServicePropagator.setServiceToScript;

@Slf4j
public class WebDriverExecutionItem<T> {

    private final Predicate<ExecutableContext> stopPredicate = executableContext -> !FeedbackStatus.COMPLETED.is(executableContext.getStatus());

    private final CompletedProcessStorage<T> completedProcessStorage;
    private final ContextManager<T> contextManager;
    private final GroovyRunnerInstance groovyRunner;
    private final WebDriver webDriver;
    private final WindowsHandler windowsHandler;

    public WebDriverExecutionItem(CompletedProcessStorage<T> completedProcessStorage,
                                  ContextManager<T> contextManager,
                                  WebDriver webDriver,
                                  WindowsHandler windowsHandler,
                                  GroovyExecutor groovyExecutor,
                                  LibrariesContainer librariesContainer) {
        this.completedProcessStorage = completedProcessStorage;
        this.webDriver = webDriver;
        this.windowsHandler = windowsHandler;
        this.contextManager = contextManager;
        this.groovyRunner = new GroovyRunnerInstance(groovyExecutor, librariesContainer,
                Arrays.asList(
                        this::setContextManagerToScript,
                        this::setStorageToScript,
                        this::setWebDriverToScript,
                        this::setWindowsHandlerToScript
                )
        );
    }

    public WebDriverProcessFeedback<T> execute(ExecutionContainer<T> executionContainer) {
        ExecutableContext<T> executableContext = executionContainer.getExecutableContext();

        log.info("Try to execute process. Process = {}.", executableContext.getProcessInformation());

        try {
            log.info("Execute process with context = {}.", executableContext);

            ScriptsExecutionItem.execute(executableContext, executionContainer.getTasks(), groovyRunner, stopPredicate);

            final FeedbackStatus feedbackStatus = FeedbackStatus._valueOf(executableContext.getStatus());

            return new WebDriverProcessFeedback<>(executableContext,
                    FeedbackStatus.COMPLETED.equals(feedbackStatus) ? FeedbackStatus.SUCCESS : feedbackStatus,
                    executableContext.getProcessInformation().getId())
                    .addSuccessProcesses(executableContext.getSuccessProcesses())
                    .addFailedProcesses(executableContext.getFailedProcesses());

        } catch (ScriptExecutionException ex) {
            log.error("Script throw exception.", ex);
            final Set<T> affectedProcesses = (Set<T>) ex.getAffectedProcesses();
            affectedProcesses.addAll(executableContext.getFailedProcesses());
            final Set<T> successProcesses = executableContext.getSuccessProcesses();

            return new WebDriverProcessFeedback<>(
                    executableContext,
                    FeedbackStatus.FAILED,
                    (T) executableContext.getProcessInformation().getId(),
                    ex
            ).addFailedProcesses(affectedProcesses).addSuccessProcesses(successProcesses);

        } catch (Exception ex) {
            log.error("Unexpected exception.", ex);
            final Set<T> successProcesses = executableContext.getSuccessProcesses();

            return new WebDriverProcessFeedback<>(
                    executableContext,
                    FeedbackStatus.FAILED,
                    (T) executableContext.getProcessInformation().getId(),
                    ex
            ).addFailedProcesses(executableContext.getFailedProcesses()).addSuccessProcesses(successProcesses);
        }
    }

    private void setWindowsHandlerToScript(Class<?> groovyClass) {
        setServiceToScript(groovyClass, InjectWindowsHandler.class, WindowsHandler.class, windowsHandler);
    }

    private void setWebDriverToScript(Class<?> groovyClass) {
        setServiceToScript(groovyClass, InjectWebDriver.class, WebDriver.class, webDriver);
    }

    private void setContextManagerToScript(Class<?> groovyClass) {
        setServiceToScript(groovyClass, InjectContextManager.class, SimpleContextManagerInMemory.class, contextManager);
    }

    private void setStorageToScript(Class<?> groovyClass) {
        setServiceToScript(groovyClass, InjectProcessStorage.class, CompletedProcessStorage.class, completedProcessStorage);
    }
}
