package com.youlfey.web.driver.engine.core.providers;

import com.youlfey.web.driver.engine.core.entity.ScopeInformation;

import java.util.Optional;

public interface ScopeProvider<ID> {

    Optional<ScopeInformation<ID>> getScope(ID scopeId);
}
