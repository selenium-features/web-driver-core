package com.youlfey.web.driver.engine.core.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import java.beans.ConstructorProperties;
import java.util.Map;
import java.util.Objects;

@Getter
@ToString
@EqualsAndHashCode
@RequiredArgsConstructor(
        onConstructor_ = @ConstructorProperties(
                {
                        "id", "processName", "conditionScenario", "mainScenario", "feedbackScenario", "additionalParameters", "options"
                }
        )
)
public class ProcessInformation<ID> {
    private final ID id;
    private final String processName;

    private final Scenario<ID> conditionScenario;
    private final Scenario<ID> mainScenario;
    private final Scenario<ID> feedbackScenario;

    private final Map<String, Object> additionalParameters;
    private final ProcessOptions options;

    public ProcessOptions getOptions() {
        if (Objects.isNull(options)) {
            return ProcessOptions.getDefaultOptions();
        }
        return options;
    }
}
