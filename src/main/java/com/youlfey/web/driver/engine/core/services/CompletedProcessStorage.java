package com.youlfey.web.driver.engine.core.services;

import java.util.Collection;

public interface CompletedProcessStorage<ID> {

    void storeFailed(Collection<ID> processIds);

    void storeSuccess(Collection<ID> processIds);

    boolean isSuccess(Collection<ID> processIds);

    boolean isFailed(Collection<ID> processIds);
}
