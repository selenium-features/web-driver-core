package com.youlfey.web.driver.engine.core.pojo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.beans.Transient;
import java.util.Optional;
import java.util.function.Function;

@RequiredArgsConstructor
@Getter
@Slf4j
public final class ConditionFeedback {
    private final boolean isAvailable;

    @JsonIgnore
    @Transient
    public <T> Optional<T> ifAvailable(Function<ConditionFeedback, T> function) {
        if (isAvailable) {
            log.info("Continue execution is available.");
            return Optional.ofNullable(function.apply(this));
        }
        log.info("Continue execution is not available.");
        return Optional.empty();
    }
}
