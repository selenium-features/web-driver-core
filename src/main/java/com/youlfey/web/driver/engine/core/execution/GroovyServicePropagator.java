package com.youlfey.web.driver.engine.core.execution;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class GroovyServicePropagator {

    @SneakyThrows
    protected static void setServiceToScript(
            Class<?> groovyClass,
            Class<? extends Annotation> annotationClass,
            Class<?> assignableClass,
            Object service) {
        log.info("Try to set service = {} class = {} in groovy = {}.", service, assignableClass, groovyClass);
        final Field[] declaredFields = groovyClass.getDeclaredFields();
        for (Field declaredField : declaredFields) {
            if (declaredField.isAnnotationPresent(annotationClass)
                    && assignableClass.isAssignableFrom(declaredField.getType())) {
                boolean isAccessible = declaredField.isAccessible();
                try {
                    if (!isAccessible) {
                        declaredField.setAccessible(true);
                    }
                    log.info("Set service = {} class = {} in groovy = {}.", service, assignableClass, groovyClass);
                    declaredField.set(null, service);
                } finally {
                    if (!isAccessible) {
                        declaredField.setAccessible(false);
                    }
                }
            }
        }
    }
}
