package com.youlfey.web.driver.engine.core.entity;

import com.youlfey.web.driver.engine.core.enums.BrowserType;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import java.beans.ConstructorProperties;
import java.util.List;
import java.util.Map;

@Getter
@ToString
@EqualsAndHashCode
@RequiredArgsConstructor(onConstructor_ = @ConstructorProperties({"id", "name", "processes", "browserType", "additionalParameters"}))
public class ScopeInformation<ID> {
    private final ID id;
    private final String name;
    private final List<ID> processes;
    private final BrowserType browserType;
    private final Map<String, Object> additionalParameters;
}
