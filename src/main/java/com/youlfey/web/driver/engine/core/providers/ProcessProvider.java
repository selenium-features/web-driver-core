package com.youlfey.web.driver.engine.core.providers;

import com.youlfey.web.driver.engine.core.entity.ProcessInformation;

import java.util.Collection;
import java.util.List;

public interface ProcessProvider<ID> {

    List<ProcessInformation<ID>> getProcessInformation(Collection<ID> processIds);
}
