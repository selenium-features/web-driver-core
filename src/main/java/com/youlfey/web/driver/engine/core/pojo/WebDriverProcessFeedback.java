package com.youlfey.web.driver.engine.core.pojo;

import com.youlfey.web.driver.engine.core.enums.FeedbackStatus;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.util.HashSet;
import java.util.Set;

@Getter
@EqualsAndHashCode
@ToString
public final class WebDriverProcessFeedback<ID> {
    private FeedbackStatus status;
    private final ID processId;
    private Throwable throwable;

    private final ExecutableContext<ID> executableContext;

    private final Set<ID> successProcesses = new HashSet<>();
    private final Set<ID> failedProcesses = new HashSet<>();

    public WebDriverProcessFeedback(ExecutableContext<ID> executableContext,
                                    FeedbackStatus status,
                                    ID processId,
                                    Throwable throwable) {
        this.executableContext = executableContext;
        this.status = status;
        this.processId = processId;
        if (FeedbackStatus.RETRY.equals(status)) {
            this.throwable = throwable;
            return;
        }
        if (FeedbackStatus.SUCCESS.equals(status)) {
            addSuccessProcess(processId);
            return;
        }
        if (FeedbackStatus.FAILED.equals(status)) {
            this.throwable = throwable;
            addFailedProcess(processId);
        }
    }

    public WebDriverProcessFeedback(ExecutableContext<ID> executableContext,
                                    FeedbackStatus status,
                                    ID processId) {
        this(executableContext, status, processId, null);
    }

    public void switchStatusFromRetryToFailed() {
        if (!FeedbackStatus.RETRY.equals(status)) return;

        status = FeedbackStatus.FAILED;
        addFailedProcess(processId);
    }

    public WebDriverProcessFeedback<ID> addSuccessProcess(ID process) {
        successProcesses.add(process);
        return this;
    }

    public WebDriverProcessFeedback<ID> addFailedProcess(ID process) {
        failedProcesses.add(process);
        return this;
    }

    public WebDriverProcessFeedback<ID> addSuccessProcesses(Set<ID> processes) {
        successProcesses.addAll(processes);
        return this;
    }

    public WebDriverProcessFeedback<ID> addFailedProcesses(Set<ID> processes) {
        failedProcesses.addAll(processes);
        return this;
    }
}
