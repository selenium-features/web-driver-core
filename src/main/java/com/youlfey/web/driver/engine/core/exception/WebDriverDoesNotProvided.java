package com.youlfey.web.driver.engine.core.exception;

import com.youlfey.web.driver.engine.core.enums.BrowserType;
import org.openqa.selenium.WebDriverException;

public class WebDriverDoesNotProvided extends WebDriverException {

    public WebDriverDoesNotProvided(BrowserType browserType) {
        super("Provider does not found for browser type '" + browserType + "'.");
    }
}
