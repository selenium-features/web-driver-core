package com.youlfey.web.driver.engine.core.context;

import com.youlfey.web.driver.engine.core.entity.ProcessInformation;
import com.youlfey.web.driver.engine.core.entity.ScopeInformation;
import lombok.*;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@EqualsAndHashCode
@ToString
public class ContextObject<T> {

    private final ScopeInformation<T> scopeInformation;
    private final ProcessInformation<T> processInformation;

    public static <T> ContextObject<T> of(ScopeInformation<T> scopeInformation, ProcessInformation<T> processInformation) {
        return new ContextObject<>(scopeInformation, processInformation);
    }
}
