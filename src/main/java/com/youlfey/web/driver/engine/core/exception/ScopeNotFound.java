package com.youlfey.web.driver.engine.core.exception;

public class ScopeNotFound extends RuntimeException {

    public ScopeNotFound(Object scopeId) {
        super("Scope not found by id = " + scopeId);
    }
}
