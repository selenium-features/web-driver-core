package com.youlfey.web.driver.engine.core.exception;

import lombok.Getter;

import java.util.Collections;
import java.util.Optional;
import java.util.Set;

public class ScriptExecutionException extends RuntimeException {
    @Getter
    private final Set affectedProcesses;
    @Getter
    private final Object process;

    public ScriptExecutionException(Set affectedProcesses, Object processId) {
        super("During execution script '" + processId + "'. Processes '" + affectedProcesses + "' affected.");
        this.affectedProcesses = Optional.ofNullable(affectedProcesses).orElse(Collections.emptySet());
        this.process = processId;
    }
}
