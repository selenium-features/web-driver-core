package com.youlfey.web.driver.engine.core;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.youlfey.web.driver.engine.core.context.ContextCloser;
import com.youlfey.web.driver.engine.core.context.ContextManager;
import com.youlfey.web.driver.engine.core.context.ContextObject;
import com.youlfey.web.driver.engine.core.drivers.SmartWebDriver;
import com.youlfey.web.driver.engine.core.entity.GroovyTask;
import com.youlfey.web.driver.engine.core.entity.IndexedContainer;
import com.youlfey.web.driver.engine.core.entity.ProcessInformation;
import com.youlfey.web.driver.engine.core.entity.ScopeInformation;
import com.youlfey.web.driver.engine.core.enums.FeedbackStatus;
import com.youlfey.web.driver.engine.core.enums.WebDriverArrivalPlace;
import com.youlfey.web.driver.engine.core.execution.ConditionExecutionItem;
import com.youlfey.web.driver.engine.core.execution.FeedbackExecutionItem;
import com.youlfey.web.driver.engine.core.execution.WebDriverExecutionItem;
import com.youlfey.web.driver.engine.core.groovy.GroovyExecutor;
import com.youlfey.web.driver.engine.core.groovy.GroovyExecutorImpl;
import com.youlfey.web.driver.engine.core.pojo.ConditionFeedback;
import com.youlfey.web.driver.engine.core.pojo.ExecutableContext;
import com.youlfey.web.driver.engine.core.pojo.ExecutionContainer;
import com.youlfey.web.driver.engine.core.pojo.WebDriverProcessFeedback;
import com.youlfey.web.driver.engine.core.services.CompletedProcessStorage;
import com.youlfey.web.driver.engine.core.services.impl.LibrariesContainer;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

@Slf4j
public class WebDriverEngine {
    private static final String THREAD_NAME = "Web driver engine - %d";
    private static final Integer COUNT_THREADS = 10;

    private final LibrariesContainer librariesContainer;
    private final GroovyExecutor groovyExecutor;
    private final ExecutorService executorService;

    public static WebDriverEngine newInstance() {
        return new WebDriverEngine();
    }

    private WebDriverEngine() {
        this.groovyExecutor = new GroovyExecutorImpl();
        this.librariesContainer = new LibrariesContainer();
        this.executorService = Executors.newFixedThreadPool(
                COUNT_THREADS,
                new ThreadFactoryBuilder()
                        .setNameFormat(THREAD_NAME)
                        .setDaemon(true)
                        .build()
        );
    }

    public WebDriverEngine registerLibrary(String libraryName, Object library) {
        librariesContainer.registerLibrary(libraryName, library);
        return this;
    }

    public <ID> List<WebDriverProcessFeedback<ID>> executeAllAvailableProcesses(ScopeContainer<ID> scopeContainer) {
        if (Objects.isNull(scopeContainer)) {
            log.info("Scope container is empty. Return immediately.");
            return Collections.emptyList();
        }

        log.info("Execute all available processes for scope = {}.", scopeContainer.getScopeId());

        final ScopeInformation<ID> scopeInformation = scopeContainer.getScopeInformation();

        if (Objects.isNull(scopeInformation)) {
            return Collections.emptyList();
        }

        final List<ProcessInformation<ID>> processes = scopeContainer.getProcesses();

        log.info("Found processes {} for scope {}.", processes, scopeInformation);

        if (CollectionUtils.isEmpty(processes)) {
            return Collections.emptyList();
        }

        List<WebDriverProcessFeedback<ID>> feedbacks = new ArrayList<>(processes.size());

        for (ProcessInformation<ID> processInformation : processes) {
            if (processInformation.getOptions().isAsync()) {
                log.info("Execute process {} async in scope {}.", processInformation, scopeInformation);
                executorService.execute(() -> executeProcessIfAvailable(scopeContainer, scopeInformation, processInformation)
                        .ifPresent(feedback -> {
                                    handleFeedback(
                                            processInformation,
                                            scopeContainer.getFeedbackTasks(processInformation),
                                            feedback,
                                            scopeContainer.getCompletedProcessStorage()
                                    );
                                    feedbacks.add(feedback);
                                }
                        )
                );
                continue;
            }
            log.info("Execute process {} sync in scope {}.", processInformation, scopeInformation);
            executeProcessIfAvailable(scopeContainer, scopeInformation, processInformation)
                    .ifPresent(feedback -> {
                                handleFeedback(
                                        processInformation,
                                        scopeContainer.getFeedbackTasks(processInformation),
                                        feedback,
                                        scopeContainer.getCompletedProcessStorage()
                                );
                                feedbacks.add(feedback);
                            }
                    );
        }

        log.info("Feedbacks {} after execution in scope {}.", feedbacks, scopeInformation);

        try {
            return feedbacks;
        } finally {
            final List<Throwable> exceptions = feedbacks.stream()
                    .map(WebDriverProcessFeedback::getThrowable)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
            log.info("Throw all exceptions for failed finished processes.");
            throwExceptionsRecursively(exceptions, 0, exceptions.size());
        }
    }

    @SneakyThrows
    private void throwExceptionsRecursively(List<Throwable> exceptions, int index, int size) {
        if (size == index) {
            return;
        }
        try {
            throw exceptions.get(index);
        } finally {
            throwExceptionsRecursively(exceptions, index + 1, size);
        }
    }

    private <ID> void handleFeedback(ProcessInformation<ID> processInformation,
                                     IndexedContainer<GroovyTask<ID>> tasks,
                                     WebDriverProcessFeedback<ID> feedback,
                                     CompletedProcessStorage<ID> storage) {
        storage.storeSuccess(feedback.getSuccessProcesses());
        storage.storeFailed(feedback.getFailedProcesses());

        if (Objects.isNull(processInformation.getFeedbackScenario())) {
            log.info("Feedback scenario does not exists for process = {}. Return immediately.", processInformation);
            return;
        }

        ExecutionContainer<ID> executionContainer = new ExecutionContainer<>(feedback.getExecutableContext(), tasks);

        new FeedbackExecutionItem<>(storage, groovyExecutor, librariesContainer)
                .process(executionContainer);
    }

    private <ID> Optional<WebDriverProcessFeedback<ID>> executeProcessIfAvailable(ScopeContainer<ID> scopeContainer,
                                                                                  ScopeInformation<ID> scopeInformation,
                                                                                  ProcessInformation<ID> processInformation) {
        ScopeContainer.WebDriverInstance webDriverInstance = scopeContainer.webDriverProvider()
                .place(processInformation.getOptions().getArrivalPlace())
                .browserType(processInformation.getOptions().getBrowserType())
                .provide();

        boolean quitIsAvailable = WebDriverArrivalPlace.REQUIRED_NEW.equals(processInformation.getOptions().getArrivalPlace());

        SmartWebDriver smartWebDriver = new SmartWebDriver(webDriverInstance.getWebDriver(), quitIsAvailable, webDriverInstance.getMainWindowId());

        ExecutableContext<ID> executableContext = new ExecutableContext<>(processInformation, scopeInformation);

        try {
            Optional<WebDriverProcessFeedback<ID>> processFeedbackOptional;
            do {
                processFeedbackOptional = performCondition(
                        scopeContainer.getCompletedProcessStorage(),
                        scopeContainer.getContextManager(),
                        smartWebDriver,
                        executableContext,
                        processInformation,
                        scopeInformation,
                        scopeContainer.getConditionTasks(processInformation)).ifAvailable(feedback ->
                        executeProcessInternal(
                                scopeContainer.getCompletedProcessStorage(),
                                scopeContainer.getContextManager(),
                                scopeInformation,
                                processInformation,
                                executableContext,
                                scopeContainer.getExecutionTasks(processInformation),
                                smartWebDriver
                        )
                );
            } while (processFeedbackOptional.isPresent() && FeedbackStatus.RETRY.equals(processFeedbackOptional.get().getStatus()));

            return processFeedbackOptional;
        } finally {
            smartWebDriver.closeAllWindows();

            // We should close web driver if driver was created
            if (WebDriverArrivalPlace.REQUIRED_NEW.equals(processInformation.getOptions().getArrivalPlace())) {
                smartWebDriver.quit();
            }
        }
    }

    private <ID> ConditionFeedback performCondition(
            CompletedProcessStorage<ID> storage,
            ContextManager<ID> contextManager,
            SmartWebDriver smartWebDriver,
            ExecutableContext<ID> executableContext,
            ProcessInformation<ID> process,
            ScopeInformation<ID> scope,
            IndexedContainer<GroovyTask<ID>> tasks
    ) {
        if (Objects.isNull(process.getConditionScenario())) {
            log.info("Condition does not exist for process {} in scope {}. Return success condition.", process, scope);
            return new ConditionFeedback(true);
        }

        ExecutionContainer<ID> executionContainer = new ExecutionContainer<>(executableContext, tasks);

        return new ConditionExecutionItem<>(storage, contextManager, smartWebDriver, smartWebDriver, groovyExecutor, librariesContainer)
                .check(executionContainer);
    }

    private <ID> WebDriverProcessFeedback<ID> executeProcessInternal(
            CompletedProcessStorage<ID> completedProcessStorage,
            ContextManager<ID> contextManager,
            ScopeInformation<ID> scopeInformation,
            ProcessInformation<ID> processInformation,
            ExecutableContext<ID> executableContext,
            IndexedContainer<GroovyTask<ID>> tasks,
            SmartWebDriver smartWebDriver) {
        try (ContextCloser<?> closeable = contextManager.context(ContextObject.of(scopeInformation, processInformation))) {

            log.info("Start process execution. Context is {}.", executableContext);

            ExecutionContainer<ID> executionContainer = new ExecutionContainer<>(executableContext, tasks);

            final WebDriverExecutionItem<ID> item = getExecutionItem(
                    smartWebDriver,
                    groovyExecutor,
                    librariesContainer,
                    completedProcessStorage,
                    contextManager
            );

            return item.execute(executionContainer);
        }
    }

    private <ID> WebDriverExecutionItem<ID> getExecutionItem(
            SmartWebDriver smartWebDriver,
            GroovyExecutor groovyExecutor,
            LibrariesContainer librariesContainer,
            CompletedProcessStorage<ID> completedProcessStorage,
            ContextManager<ID> contextManager
    ) {

        return new WebDriverExecutionItem<>(
                completedProcessStorage,
                contextManager,
                smartWebDriver,
                smartWebDriver,
                groovyExecutor,
                librariesContainer
        );
    }
}
