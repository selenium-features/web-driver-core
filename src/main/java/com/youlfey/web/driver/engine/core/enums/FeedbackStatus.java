package com.youlfey.web.driver.engine.core.enums;

import lombok.RequiredArgsConstructor;

import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;

import static java.util.stream.Collectors.toMap;

@RequiredArgsConstructor
public enum FeedbackStatus {
    SUCCESS("OK"),
    COMPLETED("Completed"),
    FAILED("Failed"),
    RETRY("Need retry");

    private final String status;

    public boolean is(String status) {
        return Objects.equals(this.status, status) || Objects.equals(this.name(), status);
    }

    private static final Map<String, FeedbackStatus> statuses = Arrays.stream(values())
            .collect(toMap(feedbackStatus -> feedbackStatus.status, Function.identity()));

    public static FeedbackStatus _valueOf(String status) {
        final FeedbackStatus feedbackStatus = statuses.get(status);
        if (Objects.isNull(feedbackStatus)) {
            try {
                return valueOf(status);
            } catch (IllegalArgumentException ex) {
                throw new IllegalArgumentException("No enum constant com.youlfey.web.driver.engine.core.enums.FeedbackStatus for " + status);
            }
        }
        return feedbackStatus;
    }
}
