package com.youlfey.web.driver.engine.core.groovy.runners;

import com.youlfey.web.driver.engine.core.dev.DevMode;
import com.youlfey.web.driver.engine.core.pojo.ExecutableContext;
import com.youlfey.web.driver.engine.core.entity.GroovyScriptBase;
import com.youlfey.web.driver.engine.core.groovy.GroovyExecutor;
import com.youlfey.web.driver.engine.core.services.LibrariesProvider;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;

import java.io.File;
import java.util.Collection;
import java.util.Objects;
import java.util.function.Consumer;

@RequiredArgsConstructor
@Slf4j
public class GroovyRunnerInstance {

    private final GroovyExecutor groovyExecutor;
    private final LibrariesProvider librariesProvider;
    private final Collection<Consumer<Class<?>>> externalEnrichers;

    public void run(GroovyScriptBase script, String scriptName, ExecutableContext context) {
        log.info("Try to run groovy script {} with context = {}.", scriptName, context);
        if (DevMode.isLoadGroovyClassFromFile() && Objects.nonNull(script.getGroovyPathFile())) {
            log.info("Dev mode is enable. Try to run script from file {}.", script.getGroovyPathFile());
            final File groovyFile = new File(script.getGroovyPathFile());
            if (groovyFile.exists()) {
                log.info("File is exists. Execute.");
                runInternal(groovyFile, context);
                return;
            }
        }
        log.info("Try to run script {} with source code.", scriptName);
        runInternal(script.getGroovyCode(), scriptName, context);
    }

    private void runInternal(String groovyCode, String scriptName, ExecutableContext executableContext) {
        final Class<?> groovyClass = groovyExecutor.getGroovyClass(groovyCode, scriptName);
        enrichWithExternalComponents(groovyClass);
        groovyExecutor.execute(groovyClass, Object.class, executableContext);
    }

    private void runInternal(File groovySourceFile, ExecutableContext executableContext) {
        final Class<?> groovyClass = groovyExecutor.getGroovyClass(groovySourceFile);
        enrichWithExternalComponents(groovyClass);
        groovyExecutor.execute(groovyClass, Object.class, executableContext);
    }


    private void enrichWithExternalComponents(Class<?> groovyClass) {
        log.info("Start enrich script {} with libraries.", groovyClass);
        librariesProvider.enrichGroovyClassWithLibraries(groovyClass);
        log.info("End enrich script {} with libraries.", groovyClass);
        if (CollectionUtils.isNotEmpty(externalEnrichers)) {
            log.info("Start enrich script with external enrichers.");
            for (Consumer<Class<?>> enricher : externalEnrichers) {
                enricher.accept(groovyClass);
            }
            log.info("End enrich script with external enrichers.");
        }
    }
}
