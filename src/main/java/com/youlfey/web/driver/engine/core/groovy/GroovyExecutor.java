package com.youlfey.web.driver.engine.core.groovy;

import java.io.File;

public interface GroovyExecutor {

    Class<?> getGroovyClass(String sourceCode,
                          String taskName);

    // Only for dev
    Class<?> getGroovyClass(File file);

    <T> T execute(Class<?> groovyClass, Class<T> returnType, T context);
}
