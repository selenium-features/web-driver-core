package com.youlfey.web.driver.engine.core.utils;

import com.youlfey.web.driver.engine.core.drivers.SmartWebDriver;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.JavascriptExecutor;

import java.util.Set;

@Slf4j
public final class TabSwitcher {

    private static final String WINDOW_OPEN_SCRIPT = "window.open()";

    public static String switchToNewTab(SmartWebDriver driver) {
            log.info("Try to switch to new tab.");
            String tab = null;

            try {
                final Set<String> tabsBeforeCreate = driver.getWindowHandles();
                ((JavascriptExecutor) driver).executeScript(WINDOW_OPEN_SCRIPT);
                final Set<String> tabsAfterCreate = driver.getWindowHandles();
                tabsAfterCreate.removeAll(tabsBeforeCreate);
                tab = tabsAfterCreate.iterator().next();
                log.info("New tab is {}.", tab);
            } finally {
                driver.switchTo().window(tab);
            }
            return tab;
    }
}
