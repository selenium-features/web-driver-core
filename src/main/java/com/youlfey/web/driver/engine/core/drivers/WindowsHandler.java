package com.youlfey.web.driver.engine.core.drivers;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;

public interface WindowsHandler {

    void closeCurrentWindow();

    void closeAllWindows();

    Set<String> getAllWindowIds();

    List<String> getAllWindowTitles();

    Set<String> getAllWindowIds(String windowTitle);

    Set<String> getAllWindowIds(Predicate<String> windowTitlePredicate);

    Optional<String> createNewWindow();

    boolean switchToWindowById(String windowId);

    Optional<String> getCurrentWindowId();
}
