package com.youlfey.web.driver.engine.core.execution;

import com.youlfey.web.driver.engine.core.pojo.ExecutableContext;
import com.youlfey.web.driver.engine.core.entity.GroovyTask;
import com.youlfey.web.driver.engine.core.entity.IndexedContainer;
import com.youlfey.web.driver.engine.core.groovy.runners.GroovyRunnerInstance;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;

import java.util.Iterator;
import java.util.function.Predicate;

@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
public class ScriptsExecutionItem<T> {

    private final ExecutableContext executableContext;
    private final IndexedContainer<GroovyTask<T>> tasksContainer;
    private final GroovyRunnerInstance groovyRunner;
    private final Predicate<ExecutableContext> stopPredicate;


    public void execute() {
        Iterator<GroovyTask<T>> iterator = tasksContainer.iterator();
        while (iterator.hasNext()) {
            GroovyTask<?> task = iterator.next();
            groovyRunner.run(task, task.getTaskName(), executableContext);

            if (stopPredicate.test(executableContext)) {
                return;
            }
        }
    }

    public static <T> void execute(ExecutableContext context,
                                   IndexedContainer<GroovyTask<T>> tasks,
                                   GroovyRunnerInstance groovyRunner,
                                   Predicate<ExecutableContext> stopPredicate) {
        new ScriptsExecutionItem<>(context, tasks, groovyRunner, stopPredicate).execute();
    }
}
