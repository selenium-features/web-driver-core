package com.youlfey.web.driver.engine.core.groovy;

import groovy.lang.Binding;
import groovy.lang.GroovyClassLoader;
import groovy.lang.GroovyCodeSource;
import groovy.lang.Script;
import lombok.SneakyThrows;
import org.codehaus.groovy.runtime.InvokerHelper;

import java.io.File;
import java.security.AccessController;
import java.security.PrivilegedAction;

import static groovy.lang.GroovyShell.DEFAULT_CODE_BASE;

public class GroovyExecutorImpl implements GroovyExecutor {

    public static final String EXECUTE = "execute";
    private final GroovyClassLoader groovyClassLoader = new GroovyClassLoader();

    @Override
    public Class<?> getGroovyClass(String scriptText,
                                 String taskName) {
        GroovyCodeSource groovyCodeSource = AccessController.doPrivileged((PrivilegedAction<GroovyCodeSource>) () ->
                new GroovyCodeSource(scriptText, taskName, DEFAULT_CODE_BASE));
        return groovyClassLoader.parseClass(groovyCodeSource);
    }

    @Override
    public <T> T execute(Class<?> groovyClass, Class<T> returnType, T context) {
        final Binding binding = getBinding();
        final Script script = InvokerHelper.createScript(groovyClass, binding);
        final Object returnValue = script.invokeMethod(EXECUTE, context);
        return returnType.cast(returnValue);
    }

    @Override
    @SneakyThrows
    public Class<?> getGroovyClass(File file) {
        GroovyCodeSource groovyCodeSource = new GroovyCodeSource(file);
        return groovyClassLoader.parseClass(groovyCodeSource);
    }

    private Binding getBinding() {
        return new Binding();
    }
}
