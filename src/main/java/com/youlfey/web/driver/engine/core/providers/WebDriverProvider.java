package com.youlfey.web.driver.engine.core.providers;

import com.youlfey.web.driver.engine.core.enums.BrowserType;
import org.openqa.selenium.WebDriver;

public interface WebDriverProvider {
    WebDriver get();

    BrowserType browserType();
}
