package com.youlfey.web.driver.engine.core.entity;

import com.youlfey.web.driver.engine.core.enums.BrowserType;
import com.youlfey.web.driver.engine.core.enums.WebDriverArrivalPlace;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import java.beans.ConstructorProperties;
import java.util.Objects;

@EqualsAndHashCode
@ToString
@RequiredArgsConstructor(onConstructor_ = @ConstructorProperties({"arrivalPlace", "browserType", "async"}))
public class ProcessOptions {

    private static final WebDriverArrivalPlace DEFAULT_PLACE = WebDriverArrivalPlace.FROM_SCOPE;

    private static final BrowserType DEFAULT_BROWSER = BrowserType.CHROME;

    private final WebDriverArrivalPlace arrivalPlace;
    private final BrowserType browserType;
    @Getter
    private final boolean async;

    public WebDriverArrivalPlace getArrivalPlace() {
        if (Objects.isNull(this.arrivalPlace)) {
            return DEFAULT_PLACE;
        }
        return arrivalPlace;
    }

    public BrowserType getBrowserType() {
        if (Objects.isNull(this.browserType)) {
            return DEFAULT_BROWSER;
        }
        return browserType;
    }

    public static ProcessOptions getDefaultOptions() {
        return new ProcessOptions(DEFAULT_PLACE, DEFAULT_BROWSER, true);
    }
}
