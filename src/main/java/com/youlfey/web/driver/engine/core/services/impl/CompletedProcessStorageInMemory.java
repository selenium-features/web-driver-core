package com.youlfey.web.driver.engine.core.services.impl;

import com.youlfey.web.driver.engine.core.services.CompletedProcessStorage;
import lombok.extern.slf4j.Slf4j;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@Slf4j
public class CompletedProcessStorageInMemory<ID> implements CompletedProcessStorage<ID> {

    private final Set<ID> failedIds = Collections.synchronizedSet(new HashSet<>());
    private final Set<ID> successIds = Collections.synchronizedSet(new HashSet<>());

    @Override
    public void storeFailed(Collection<ID> processIds) {
        log.info("Store failed processes = {}. Evict success processes = {}.", processIds, processIds);
        failedIds.addAll(processIds);
        successIds.removeAll(processIds);
    }

    @Override
    public void storeSuccess(Collection<ID> processIds) {
        log.info("Store success processes = {}. Evict failed processes = {}.", processIds, processIds);
        successIds.addAll(processIds);
        failedIds.removeAll(processIds);
    }

    @Override
    public boolean isSuccess(Collection<ID> processIds) {
        return successIds.containsAll(processIds);
    }

    @Override
    public boolean isFailed(Collection<ID> processIds) {
        return failedIds.containsAll(processIds);
    }
}
