package com.youlfey.web.driver.engine.core.enums;

public enum WebDriverArrivalPlace {
    FROM_SCOPE, REQUIRED_NEW
}
