package com.youlfey.web.driver.engine.core.drivers;


import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.Logs;

import java.net.URL;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.*;
import java.util.stream.Collectors;

import static java.util.Collections.emptyList;
import static java.util.Collections.emptySet;
import static java.util.stream.Collectors.toSet;

/**
 * Smart web driver
 */
@Slf4j
public class SmartWebDriver implements WebDriver, JavascriptExecutor, TakesScreenshot, WindowsHandler {
    private static final String WINDOW_OPEN_SCRIPT = "window.open()";
    private static final String DELAY_PROPERTY_NAME = "web.driver.delay.internal.ms";
    private static final Long DELAY_DEFAULT = 25L;

    @NonNull
    private volatile WebDriver webDriver;

    private final Object mutex;

    private final WindowProcessor windowProcessor;
    private final SyncProcessor syncProcessor;
    private final DelayProcessor delayProcessor;
    private final SmartProcessor smartProcessor;

    private final Boolean quitIsAvailable;
    private final String notClosableWindow;

    public SmartWebDriver(WebDriver webDriver, Boolean quitIsAvailable, String notClosableWindow) {
        this.webDriver = webDriver;
        this.mutex = webDriver;
        this.windowProcessor = new WindowProcessor(webDriver);
        this.syncProcessor = new SyncProcessor(this.mutex);
        this.delayProcessor = new DelayProcessor(getDelayMs());
        this.smartProcessor = new SmartProcessor(delayProcessor, windowProcessor, syncProcessor);
        this.quitIsAvailable = quitIsAvailable;
        this.notClosableWindow = notClosableWindow;

        // Need to prevent NPE during open new windows
        this.smartProcessor.process(() -> webDriver.switchTo().window(notClosableWindow));
    }

    private long getDelayMs() {
        return Long.getLong(DELAY_PROPERTY_NAME, DELAY_DEFAULT);
    }

    private static List<WebElement> convertToSmart(List<WebElement> webElements, SmartProcessor smartProcessor) {
        if (CollectionUtils.isEmpty(webElements)) {
            return webElements;
        }

        return webElements.stream()
                .map(webElement -> convertToSmart(webElement, smartProcessor))
                .collect(Collectors.toList());
    }

    private static SmartWebElement convertToSmart(WebElement webElement, SmartProcessor smartProcessor) {
        return new SmartWebElement(webElement, smartProcessor);
    }

    @Override
    public Object executeScript(String s, Object... objects) {
        try {
            JavascriptExecutor javascriptExecutor = (JavascriptExecutor) this.webDriver;
            return smartProcessor.process(javascriptExecutor::executeScript, s, objects);
        } catch (Throwable ex) {
            throw new WebDriverException(ex);
        }
    }

    @Override
    public Object executeAsyncScript(String s, Object... objects) {
        try {
            JavascriptExecutor javascriptExecutor = (JavascriptExecutor) this.webDriver;
            return smartProcessor.process(javascriptExecutor::executeAsyncScript, s, objects);
        } catch (Throwable ex) {
            throw new WebDriverException(ex);
        }
    }

    @Override
    public <X> X getScreenshotAs(OutputType<X> outputType) throws WebDriverException {
        try {
            TakesScreenshot takesScreenshot = (TakesScreenshot) this.webDriver;
            return smartProcessor.process((Function<OutputType<X>, X>) takesScreenshot::getScreenshotAs, outputType);
        } catch (Throwable ex) {
            throw new WebDriverException(ex);
        }
    }

    @RequiredArgsConstructor(access = AccessLevel.PRIVATE)
    private static final class WindowProcessor {

        @EqualsAndHashCode
        @Getter
        @ToString
        private static final class WindowsContainer {
            private static final class WindowInstance {
                private final String windowId;
                private final String windowTitle;
                private final Long initializedAt;

                private WindowInstance(String windowId, String windowTitle) {
                    this.windowId = windowId;
                    this.windowTitle = windowTitle;
                    this.initializedAt = new Date().getTime();
                }
            }

            private final List<WindowInstance> windows = new ArrayList<>();

            private void addWindow(WindowInstance windowInstance) {
                synchronized (windows) {
                    addWindowInternal(windowInstance);
                }
            }

            private void addWindow(String windowId, String windowTitle) {
                addWindow(new WindowInstance(windowId, windowTitle));
            }

            private void addWindowInternal(WindowInstance windowInstance) {
                windows.removeIf(registeredTabInstance -> registeredTabInstance.windowId.equals(windowInstance.windowId));
                windows.add(windowInstance);
            }

            private Optional<WindowInstance> getLastAddedWindow() {
                synchronized (windows) {
                    if (isEmpty()) {
                        return Optional.empty();
                    }
                    return windows.stream().min((left, right) -> Long.compare(right.initializedAt, left.initializedAt));
                }
            }

            private Optional<String> getLastAddedWindowId() {
                synchronized (windows) {
                    return getLastAddedWindow().map(windowInstance -> windowInstance.windowId);
                }
            }

            private void removeLastAddedWindow() {
                synchronized (windows) {
                    if (!isEmpty()) {
                        getLastAddedWindow().ifPresent(windows::remove);
                    }
                }
            }

            private void removeWindowsByIds(Set<String> windows) {
                synchronized (this.windows) {
                    if (!isEmpty() && CollectionUtils.isNotEmpty(windows)) {
                        this.windows.removeIf(registeredTabInstance -> windows.contains(registeredTabInstance.windowId));
                    }
                }
            }

            private void addWindows(Set<WindowInstance> windows) {
                synchronized (this.windows) {
                    if (CollectionUtils.isNotEmpty(windows)) {
                        windows.forEach(this::addWindowInternal);
                    }
                }
            }

            private boolean lastAddedWindowIsById(String windowsId) {
                synchronized (windows) {
                    final Optional<WindowInstance> lastTab = getLastAddedWindow();
                    return lastTab.isPresent() && Objects.equals(lastTab.get().windowId, windowsId);
                }
            }

            private boolean isEmpty() {
                synchronized (windows) {
                    return CollectionUtils.isEmpty(windows);
                }
            }

            private Set<String> getAllWindowsIds(Predicate<String> titlePredicate) {
                if (isEmpty()) {
                    return emptySet();
                }
                return windows.stream()
                        .filter(windowInstance -> titlePredicate.test(windowInstance.windowTitle))
                        .map(windowInstance -> windowInstance.windowId)
                        .collect(toSet());
            }

            private List<String> getAllWindowsTitles() {
                if (isEmpty()) {
                    return emptyList();
                }
                return windows.stream()
                        .map(windowInstance -> windowInstance.windowTitle)
                        .collect(Collectors.toList());
            }
        }

        private final WebDriver webDriver;
        private final WindowsContainer windowsContainer = new WindowsContainer();

        private Optional<String> getCurrentWindow() {
            try {
                return Optional.ofNullable(webDriver.getWindowHandle());
            } catch (WebDriverException ex) {
                log.error("Error during get current window.", ex);
                return Optional.empty();
            }
        }

        protected <T> T process(Supplier<T> action) {
            Set<String> windowsBefore = webDriver.getWindowHandles();
            try {
                return processInternal(action);
            } finally {
                handleAfterExecution(windowsBefore);
            }
        }

        protected <T, E> E process(Function<T, E> action, T input) {
            Set<String> windowsBefore = webDriver.getWindowHandles();
            try {
                return processInternal(action, input);
            } finally {
                handleAfterExecution(windowsBefore);
            }
        }

        protected <L, R, E> E process(BiFunction<L, R, E> action, L left, R right) {
            Set<String> windowsBefore = webDriver.getWindowHandles();
            try {
                return processInternal(action, left, right);
            } finally {
                handleAfterExecution(windowsBefore);
            }
        }

        protected <T> void process(Consumer<T> action, T input) {
            Set<String> windowsBefore = webDriver.getWindowHandles();
            try {
                processInternal(action, input);
            } finally {
                handleAfterExecution(windowsBefore);
            }
        }

        protected void process(Runnable action) {
            Set<String> windowsBefore = webDriver.getWindowHandles();
            try {
                processInternal(action, false);
            } finally {
                handleAfterExecution(windowsBefore);
            }
        }

        protected void process(Runnable action, boolean disableSwitchToSourceWindow) {
            Set<String> windowsBefore = webDriver.getWindowHandles();
            try {
                processInternal(action, disableSwitchToSourceWindow);
            } finally {
                handleAfterExecution(windowsBefore);
            }
        }

        private void handleAfterExecution(Set<String> windowsBefore) {
            Set<String> windowsAfter = webDriver.getWindowHandles();
            Set<String> closedWindows = getClosedWindows(windowsBefore, windowsAfter);
            Set<String> openedWindows = getOpenedWindows(windowsBefore, windowsAfter);

            if (CollectionUtils.isNotEmpty(closedWindows)) {
                windowsContainer.removeWindowsByIds(closedWindows);
            }

            if (CollectionUtils.isNotEmpty(openedWindows)) {
                Optional<String> currentWindow = getCurrentWindow();
                try {
                    Set<WindowsContainer.WindowInstance> windowInstances = openedWindows.stream()
                            .map(windowId -> {
                                String windowTitle = webDriver.switchTo().window(windowId).getTitle();
                                return new WindowsContainer.WindowInstance(windowId, windowTitle);
                            })
                            .collect(toSet());

                    windowsContainer.addWindows(windowInstances);
                } finally {
                    currentWindow.ifPresent(s -> webDriver.switchTo().window(s));
                }
            }
        }

        private <T> T processInternal(Supplier<T> action) {
            Optional<String> currentWindow = getCurrentWindow();
            if (isNotNeedToSwitch(currentWindow.orElse(null))) {
                return action.get();
            }
            try {
                windowsContainer.getLastAddedWindowId().ifPresent(tab -> webDriver.switchTo().window(tab));
                return action.get();
            } finally {
                currentWindow.ifPresent(s -> webDriver.switchTo().window(s));
            }
        }

        protected <T, E> E processInternal(Function<T, E> action, T input) {
            Optional<String> currentWindow = getCurrentWindow();
            if (isNotNeedToSwitch(currentWindow.orElse(null))) {
                return action.apply(input);
            }
            try {
                windowsContainer.getLastAddedWindowId().ifPresent(tab -> webDriver.switchTo().window(tab));
                return action.apply(input);
            } finally {
                currentWindow.ifPresent(s -> webDriver.switchTo().window(s));
            }
        }

        protected <L, R, E> E processInternal(BiFunction<L, R, E> action, L left, R right) {
            Optional<String> currentWindow = getCurrentWindow();
            if (isNotNeedToSwitch(currentWindow.orElse(null))) {
                return action.apply(left, right);
            }
            try {
                windowsContainer.getLastAddedWindowId().ifPresent(tab -> webDriver.switchTo().window(tab));
                return action.apply(left, right);
            } finally {
                currentWindow.ifPresent(s -> webDriver.switchTo().window(s));
            }
        }

        protected <T> void processInternal(Consumer<T> action, T input) {
            Optional<String> currentWindow = getCurrentWindow();
            if (isNotNeedToSwitch(currentWindow.orElse(null))) {
                action.accept(input);
                return;
            }
            try {
                windowsContainer.getLastAddedWindowId().ifPresent(tab -> webDriver.switchTo().window(tab));
                action.accept(input);
            } finally {
                currentWindow.ifPresent(s -> webDriver.switchTo().window(s));
            }
        }

        protected void processInternal(Runnable action, boolean disableSwitchToSourceWindow) {
            Optional<String> currentWindow = getCurrentWindow();
            if (isNotNeedToSwitch(currentWindow.orElse(null))) {
                action.run();
                return;
            }
            try {
                windowsContainer.getLastAddedWindowId().ifPresent(tab -> webDriver.switchTo().window(tab));
                action.run();
            } finally {
                if (!disableSwitchToSourceWindow) {
                    currentWindow.ifPresent(s -> webDriver.switchTo().window(s));
                }
            }
        }

        private boolean isNotNeedToSwitch(String currentWindow) {
            synchronized (windowsContainer) {
                return windowsContainer.isEmpty() || windowsContainer.lastAddedWindowIsById(currentWindow);
            }
        }

        private static Set<String> getClosedWindows(Set<String> windowsBefore, Set<String> windowsAfter) {
            Set<String> closedWindows = new HashSet<>(windowsBefore);
            closedWindows.removeAll(windowsAfter);
            return closedWindows;
        }

        private static Set<String> getOpenedWindows(Set<String> windowsBefore, Set<String> windowsAfter) {
            Set<String> openedWindows = new HashSet<>(windowsAfter);
            openedWindows.removeAll(windowsBefore);
            return openedWindows;
        }
    }

    @RequiredArgsConstructor(access = AccessLevel.PRIVATE)
    private static final class SyncProcessor {
        private final Object mutex;

        protected <T> T process(Supplier<T> action) {
            synchronized (mutex) {
                return action.get();
            }
        }

        protected <T, E> E process(Function<T, E> action, T input) {
            synchronized (mutex) {
                return action.apply(input);
            }
        }

        protected <L, R, E> E process(BiFunction<L, R, E> action, L left, R right) {
            synchronized (mutex) {
                return action.apply(left, right);
            }
        }

        protected <T> void process(Consumer<T> action, T input) {
            synchronized (mutex) {
                action.accept(input);
            }
        }

        protected void process(Runnable action) {
            synchronized (mutex) {
                action.run();
            }
        }
    }

    private static final class DelayProcessor {
        private final Long millisecondsBefore;
        private final Long millisecondsAfter;

        private DelayProcessor(Long millisecondsBefore, Long millisecondsAfter) {
            this.millisecondsBefore = millisecondsBefore;
            this.millisecondsAfter = millisecondsAfter;
        }

        public DelayProcessor(Long milliseconds) {
            this.millisecondsBefore = milliseconds;
            this.millisecondsAfter = milliseconds;
        }

        @SneakyThrows
        protected <T> T process(Supplier<T> action) {
            try {
                if (millisecondsBefore > 0) {
                    Thread.sleep(millisecondsBefore);
                }
                return action.get();
            } finally {
                if (millisecondsAfter > 0) {
                    Thread.sleep(millisecondsAfter);
                }
            }
        }

        @SneakyThrows
        protected <T, E> E process(Function<T, E> action, T input) {
            try {
                if (millisecondsBefore > 0) {
                    Thread.sleep(millisecondsBefore);
                }
                return action.apply(input);
            } finally {
                if (millisecondsAfter > 0) {
                    Thread.sleep(millisecondsAfter);
                }
            }
        }

        @SneakyThrows
        protected <L, R, E> E process(BiFunction<L, R, E> action, L left, R right) {
            try {
                if (millisecondsBefore > 0) {
                    Thread.sleep(millisecondsBefore);
                }
                return action.apply(left, right);
            } finally {
                if (millisecondsAfter > 0) {
                    Thread.sleep(millisecondsAfter);
                }
            }
        }

        @SneakyThrows
        protected <T> void process(Consumer<T> action, T input) {
            try {
                if (millisecondsBefore > 0) {
                    Thread.sleep(millisecondsBefore);
                }
                action.accept(input);
            } finally {
                if (millisecondsAfter > 0) {
                    Thread.sleep(millisecondsAfter);
                }
            }
        }

        @SneakyThrows
        protected void process(Runnable action) {
            try {
                if (millisecondsBefore > 0) {
                    Thread.sleep(millisecondsBefore);
                }
                action.run();
            } finally {
                if (millisecondsAfter > 0) {
                    Thread.sleep(millisecondsAfter);
                }
            }
        }
    }

    @RequiredArgsConstructor(access = AccessLevel.PRIVATE)
    private static final class SmartProcessor {
        private final DelayProcessor delayProcessor;
        private final WindowProcessor windowProcessor;
        private final SyncProcessor syncProcessor;


        protected <T> T process(Supplier<T> action) {
            return syncProcessor.process(() ->
                    windowProcessor.process(() ->
                            delayProcessor.process(action)));
        }

        protected <T, E> E process(Function<T, E> action, T input) {
            return syncProcessor.process(() ->
                    windowProcessor.process(() ->
                            delayProcessor.process(action, input)));
        }

        protected <L, R, E> E process(BiFunction<L, R, E> action, L left, R right) {
            return syncProcessor.process(() ->
                    windowProcessor.process(() ->
                            delayProcessor.process(action, left, right)));
        }

        protected <T> void process(Consumer<T> action, T input) {
            syncProcessor.process(() ->
                    windowProcessor.process(() ->
                            delayProcessor.process(action, input)));
        }

        protected void process(Runnable action) {
            syncProcessor.process(() ->
                    windowProcessor.process(() ->
                            delayProcessor.process(action)));
        }
    }

    @Override
    public void get(String url) {
        smartProcessor.process(webDriver::get, url);
    }

    @Override
    public String getCurrentUrl() {
        return smartProcessor.process(webDriver::getCurrentUrl);
    }

    @Override
    public String getTitle() {
        return smartProcessor.process(webDriver::getTitle);
    }

    @Override
    public List<WebElement> findElements(By by) {
        return convertToSmart(smartProcessor.process(webDriver::findElements, by), smartProcessor);
    }

    @Override
    public WebElement findElement(By by) {
        return convertToSmart(smartProcessor.process(webDriver::findElement, by), smartProcessor);
    }

    @Override
    public String getPageSource() {
        return smartProcessor.process(webDriver::getPageSource);
    }

    @Override
    public void close() {
        syncProcessor.process(() ->
                windowProcessor.process(() ->
                        delayProcessor.process(() -> {
                            windowProcessor.windowsContainer.getLastAddedWindowId()
                                    .ifPresent(windowsId -> {
                                        if (windowsId.equals(notClosableWindow)) {
                                            throw new UnsupportedCommandException("Cannot close the not closeable window.");
                                        }
                                    });
                            webDriver.close();
                            windowProcessor.windowsContainer.removeLastAddedWindow();
                        }))
        );
    }

    @Override
    public void closeAllWindows() {
        while (!windowProcessor.windowsContainer.isEmpty()) {
            close();
        }
    }

    @Override
    public void closeCurrentWindow() {
        this.close();
    }

    @Override
    public Set<String> getAllWindowIds() {
        return windowProcessor.windowsContainer.getAllWindowsIds(s -> true);
    }

    @Override
    public List<String> getAllWindowTitles() {
        return windowProcessor.windowsContainer.getAllWindowsTitles();
    }

    @Override
    public Set<String> getAllWindowIds(String windowTitle) {
        return windowProcessor.windowsContainer.getAllWindowsIds(windowTitle::equals);
    }

    @Override
    public Set<String> getAllWindowIds(Predicate<String> windowTitlePredicate) {
        return windowProcessor.windowsContainer.getAllWindowsIds(windowTitlePredicate);
    }

    @Override
    public Optional<String> createNewWindow() {
        ((JavascriptExecutor) this).executeScript(WINDOW_OPEN_SCRIPT);
        return windowProcessor.windowsContainer.getLastAddedWindowId();
    }

    @Override
    public boolean switchToWindowById(String windowId) {
        try {
            this.switchTo().window(windowId);
            return true;
        } catch (Exception ex) {
            log.error("The error occurred during switching", ex);
            return false;
        }
    }

    @Override
    public Optional<String> getCurrentWindowId() {
        return windowProcessor.windowsContainer.getLastAddedWindowId();
    }

    @Override
    public void quit() {
        if (quitIsAvailable) {
            smartProcessor.process(webDriver::quit);
            return;
        }
        throw new UnsupportedCommandException("Quit is not available by configuration");
    }

    @Override
    public Set<String> getWindowHandles() {
        return smartProcessor.process(webDriver::getWindowHandles);
    }

    @Override
    public String getWindowHandle() {
        return smartProcessor.process(webDriver::getWindowHandle);
    }

    @Override
    public TargetLocator switchTo() {
        return new SmartTargetLocator(
                notClosableWindow,
                webDriver,
                smartProcessor,
                this,
                smartProcessor.process(webDriver::switchTo)
        );
    }

    @Override
    public Navigation navigate() {
        return new SmartNavigation(smartProcessor.process(webDriver::navigate), smartProcessor);
    }

    @Override
    public Options manage() {
        return new SmartOptions(smartProcessor.process(webDriver::manage), smartProcessor);
    }

    @RequiredArgsConstructor(access = AccessLevel.PRIVATE)
    private static final class SmartTargetLocator implements TargetLocator {

        private final String notClosableWindow;
        private final WebDriver webDriver;
        private final SmartProcessor smartProcessor;
        private final SmartWebDriver smartWebDriver;
        private final TargetLocator targetLocator;

        @Override
        public WebDriver frame(int index) {
            smartProcessor.process((Function<Integer, WebDriver>) targetLocator::frame, index);
            return smartWebDriver;
        }

        @Override
        public WebDriver frame(String nameOrId) {
            smartProcessor.process((Function<String, WebDriver>) targetLocator::frame, nameOrId);
            return smartWebDriver;
        }

        @Override
        public WebDriver frame(WebElement frameElement) {
            smartProcessor.process((Function<WebElement, WebDriver>) targetLocator::frame, frameElement);
            return smartWebDriver;
        }

        @Override
        public WebDriver parentFrame() {
            smartProcessor.process(targetLocator::parentFrame);
            return smartWebDriver;
        }

        @Override
        public WebDriver window(String nameOrHandle) {
            if (notClosableWindow.equals(nameOrHandle)) {
                throw new UnsupportedCommandException("Cannot switch to not closeable window.");
            }
            smartProcessor.syncProcessor.process(() -> smartProcessor.windowProcessor.process(() ->
                    smartProcessor.delayProcessor.process(() -> {
                        targetLocator.window(nameOrHandle);
                        smartProcessor.windowProcessor.windowsContainer.addWindow(nameOrHandle, webDriver.getTitle());
                    }), true)
            );
            return smartWebDriver;
        }

        @Override
        public WebDriver defaultContent() {
            smartProcessor.process(targetLocator::defaultContent);
            return smartWebDriver;
        }

        @Override
        public WebElement activeElement() {
            return new SmartWebElement(smartProcessor.process(targetLocator::activeElement), smartProcessor);
        }

        @Override
        public Alert alert() {
            return new SmartAlert(smartProcessor.process(targetLocator::alert), smartProcessor);
        }
    }

    @RequiredArgsConstructor(access = AccessLevel.PRIVATE)
    private static final class SmartWebElement implements WebElement {

        private final WebElement webElement;
        private final SmartProcessor smartProcessor;

        @Override
        public void click() {
            smartProcessor.process(webElement::click);
        }

        @Override
        public void submit() {
            smartProcessor.process(webElement::submit);
        }

        @Override
        public void sendKeys(CharSequence... charSequences) {
            smartProcessor.process((Consumer<CharSequence[]>) webElement::sendKeys, charSequences);
        }

        @Override
        public void clear() {
            smartProcessor.process(webElement::clear);
        }

        @Override
        public String getTagName() {
            return smartProcessor.process(webElement::getTagName);
        }

        @Override
        public String getAttribute(String s) {
            return smartProcessor.process(webElement::getAttribute, s);
        }

        @Override
        public boolean isSelected() {
            return smartProcessor.process(webElement::isSelected);
        }

        @Override
        public boolean isEnabled() {
            return smartProcessor.process(webElement::isEnabled);
        }

        @Override
        public String getText() {
            return smartProcessor.process(webElement::getText);
        }

        @Override
        public List<WebElement> findElements(By by) {
            return convertToSmart(smartProcessor.process(webElement::findElements, by), smartProcessor);
        }

        @Override
        public WebElement findElement(By by) {
            return convertToSmart(smartProcessor.process(webElement::findElement, by), smartProcessor);
        }

        @Override
        public boolean isDisplayed() {
            return smartProcessor.process(webElement::isDisplayed);
        }

        @Override
        public Point getLocation() {
            return smartProcessor.process(webElement::getLocation);
        }

        @Override
        public Dimension getSize() {
            return smartProcessor.process(webElement::getSize);
        }

        @Override
        public Rectangle getRect() {
            return smartProcessor.process(webElement::getRect);
        }

        @Override
        public String getCssValue(String s) {
            return smartProcessor.process(webElement::getCssValue, s);
        }

        @Override
        public <X> X getScreenshotAs(OutputType<X> outputType) throws WebDriverException {
            return smartProcessor.process((Function<OutputType<X>, X>) webElement::getScreenshotAs, outputType);
        }
    }

    @RequiredArgsConstructor(access = AccessLevel.PRIVATE)
    private static final class SmartNavigation implements Navigation {

        private final Navigation navigation;
        private final SmartProcessor smartProcessor;

        @Override
        public void back() {
            smartProcessor.process(navigation::back);
        }

        @Override
        public void forward() {
            smartProcessor.process(navigation::back);
        }

        @Override
        public void to(String s) {
            smartProcessor.process((Consumer<String>) navigation::to, s);
        }

        @Override
        public void to(URL url) {
            smartProcessor.process((Consumer<URL>) navigation::to, url);
        }

        @Override
        public void refresh() {
            smartProcessor.process(navigation::refresh);
        }
    }

    @RequiredArgsConstructor(access = AccessLevel.PRIVATE)
    private static final class SmartOptions implements Options {

        private final Options options;
        private final SmartProcessor smartProcessor;

        @Override
        public void addCookie(Cookie cookie) {
            smartProcessor.process(options::addCookie, cookie);
        }

        @Override
        public void deleteCookieNamed(String s) {
            smartProcessor.process(options::deleteCookieNamed, s);
        }

        @Override
        public void deleteCookie(Cookie cookie) {
            smartProcessor.process(options::deleteCookie, cookie);
        }

        @Override
        public void deleteAllCookies() {
            smartProcessor.process(options::deleteAllCookies);
        }

        @Override
        public Set<Cookie> getCookies() {
            return smartProcessor.process(options::getCookies);
        }

        @Override
        public Cookie getCookieNamed(String s) {
            return smartProcessor.process(options::getCookieNamed, s);
        }

        @Override
        public Timeouts timeouts() {
            return new SmartTimeouts(smartProcessor.process(options::timeouts), smartProcessor);
        }

        @Override
        public ImeHandler ime() {
            return new SmartImeHandler(smartProcessor.process(options::ime), smartProcessor);
        }

        @Override
        public Window window() {
            return new SmartWindow(smartProcessor.process(options::window), smartProcessor);
        }

        @Override
        public Logs logs() {
            return new SmartLogs(smartProcessor.process(options::logs), smartProcessor);
        }
    }

    @RequiredArgsConstructor(access = AccessLevel.PRIVATE)
    private static final class SmartTimeouts implements Timeouts {

        private final Timeouts timeouts;
        private final SmartProcessor smartProcessor;

        @Override
        public Timeouts implicitlyWait(long l, TimeUnit timeUnit) {
            smartProcessor.process(timeouts::implicitlyWait, l, timeUnit);
            return this;
        }

        @Override
        public Timeouts setScriptTimeout(long l, TimeUnit timeUnit) {
            smartProcessor.process(timeouts::setScriptTimeout, l, timeUnit);
            return this;
        }

        @Override
        public Timeouts pageLoadTimeout(long l, TimeUnit timeUnit) {
            smartProcessor.process(timeouts::pageLoadTimeout, l, timeUnit);
            return this;
        }
    }

    @RequiredArgsConstructor(access = AccessLevel.PRIVATE)
    private static final class SmartImeHandler implements ImeHandler {
        private final ImeHandler imeHandler;
        private final SmartProcessor smartProcessor;

        @Override
        public List<String> getAvailableEngines() {
            return smartProcessor.process(imeHandler::getAvailableEngines);
        }

        @Override
        public String getActiveEngine() {
            return smartProcessor.process(imeHandler::getActiveEngine);
        }

        @Override
        public boolean isActivated() {
            return smartProcessor.process(imeHandler::isActivated);
        }

        @Override
        public void deactivate() {
            smartProcessor.process(imeHandler::deactivate);
        }

        @Override
        public void activateEngine(String s) {
            smartProcessor.process(imeHandler::activateEngine, s);
        }
    }

    @RequiredArgsConstructor(access = AccessLevel.PRIVATE)
    private static final class SmartWindow implements Window {

        private final Window window;
        private final SmartProcessor smartProcessor;

        @Override
        public void setSize(Dimension dimension) {
            smartProcessor.process(window::setSize, dimension);
        }

        @Override
        public void setPosition(Point point) {
            smartProcessor.process(window::setPosition, point);
        }

        @Override
        public Dimension getSize() {
            return smartProcessor.process(window::getSize);
        }

        @Override
        public Point getPosition() {
            return smartProcessor.process(window::getPosition);
        }

        @Override
        public void maximize() {
            smartProcessor.process(window::maximize);
        }

        @Override
        public void fullscreen() {
            smartProcessor.process(window::fullscreen);
        }
    }

    @RequiredArgsConstructor(access = AccessLevel.PRIVATE)
    private static final class SmartLogs implements Logs {
        private final Logs logs;
        private final SmartProcessor smartProcessor;

        @Override
        public LogEntries get(String s) {
            return smartProcessor.process(logs::get, s);
        }

        @Override
        public Set<String> getAvailableLogTypes() {
            return smartProcessor.process(logs::getAvailableLogTypes);
        }
    }

    @RequiredArgsConstructor(access = AccessLevel.PRIVATE)
    private static final class SmartAlert implements Alert {
        private final Alert alert;
        private final SmartProcessor smartProcessor;

        @Override
        public void dismiss() {
            smartProcessor.process(alert::dismiss);
        }

        @Override
        public void accept() {
            smartProcessor.process(alert::accept);
        }

        @Override
        public String getText() {
            return smartProcessor.process(alert::getText);
        }

        @Override
        public void sendKeys(String s) {
            smartProcessor.process(alert::sendKeys, s);
        }
    }
}
