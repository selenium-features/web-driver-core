package com.youlfey.web.driver.engine.core.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.beans.ConstructorProperties;

@Getter
@EqualsAndHashCode(callSuper = true)
public class GroovyTask<ID> extends GroovyScriptBase {
    private final ID id;
    private final String taskName;

    @ConstructorProperties({"id", "taskName", "groovyCode"})
    public GroovyTask(ID id, String taskName, String groovyCode) {
        super(groovyCode);
        this.id = id;
        this.taskName = taskName;
    }

    @Override
    public String toString() {
        return "GroovyTask{" +
                "id=" + id +
                ", taskName='" + taskName + '\'' +
                '}';
    }
}
