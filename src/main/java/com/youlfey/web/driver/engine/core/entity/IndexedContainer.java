package com.youlfey.web.driver.engine.core.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import org.apache.commons.collections4.MapUtils;

import java.beans.ConstructorProperties;
import java.beans.Transient;
import java.util.*;
import java.util.stream.Collectors;

@Getter
@EqualsAndHashCode
@ToString
public class IndexedContainer<Type> {
    @Getter
    @JsonValue
    private final Map<Integer, Type> container;

    @ConstructorProperties({"container"})
    public IndexedContainer(Map<Integer, Type> container) {
        final HashMap<Integer, Type> draftContainer = new HashMap<>();
        if (MapUtils.isEmpty(container)) {
            this.container = Collections.unmodifiableMap(draftContainer);
            return;
        }
        final List<Integer> indexes = container.keySet().stream().sorted().collect(Collectors.toList());
        for (int i = 0; i < indexes.size(); i++) {
            draftContainer.put(i, container.get(indexes.get(i)));
        }
        this.container = Collections.unmodifiableMap(draftContainer);
    }

    public static boolean isEmpty(IndexedContainer<?> container) {
        return container == null || container.getContainer() == null || container.getContainer().size() == 0;
    }

    public static <T> IndexedContainer<T> empty() {
        return new IndexedContainer<>(null);
    }

    @JsonIgnore
    public boolean isEmpty() {
        return MapUtils.isEmpty(container);
    }

    @JsonIgnore
    @Transient
    public Iterator<Type> iterator() {
        return new ContainerIterator<>(this.container);
    }

    private static final class ContainerIterator<Type> implements Iterator<Type> {

        private Integer currentIndex = 0;
        private final Map<Integer, Type> tasks;
        private final List<Integer> indexes;

        private ContainerIterator(Map<Integer, Type> tasks) {
            this.tasks = Collections.unmodifiableMap(Optional.ofNullable(tasks).orElse(Collections.emptyMap()));
            this.indexes = Collections.unmodifiableList(
                    this.tasks.keySet().stream()
                            .sorted(Comparator.comparingInt(i -> i))
                            .collect(Collectors.toList())
            );
        }

        @Override
        public boolean hasNext() {
            return tasks.size() != 0 && currentIndex < indexes.size();
        }

        @Override
        public Type next() {
            return tasks.get(indexes.get(currentIndex++));
        }
    }
}
