package com.youlfey.web.driver.engine.core.execution;

import com.youlfey.web.driver.engine.core.annotation.InjectProcessStorage;
import com.youlfey.web.driver.engine.core.groovy.GroovyExecutor;
import com.youlfey.web.driver.engine.core.groovy.runners.GroovyRunnerInstance;
import com.youlfey.web.driver.engine.core.pojo.ExecutableContext;
import com.youlfey.web.driver.engine.core.pojo.ExecutionContainer;
import com.youlfey.web.driver.engine.core.services.CompletedProcessStorage;
import com.youlfey.web.driver.engine.core.services.impl.LibrariesContainer;
import lombok.extern.slf4j.Slf4j;

import java.util.Collections;
import java.util.function.Predicate;

import static com.youlfey.web.driver.engine.core.execution.GroovyServicePropagator.setServiceToScript;

@Slf4j
public class FeedbackExecutionItem<T> {

    private final CompletedProcessStorage<T> completedProcessStorage;
    private final GroovyRunnerInstance groovyRunner;

    private final Predicate<ExecutableContext> predicate = context -> false;

    public FeedbackExecutionItem(CompletedProcessStorage<T> completedProcessStorage,
                                 GroovyExecutor groovyExecutor,
                                 LibrariesContainer librariesContainer) {
        this.completedProcessStorage = completedProcessStorage;
        this.groovyRunner = new GroovyRunnerInstance(groovyExecutor, librariesContainer,
                Collections.singletonList(this::setStorageToScript));
    }

    public void process(ExecutionContainer<T> executionContainer) {
        try {
            log.info("Try to process feedback. Context = {}.", executionContainer.getExecutableContext());
            ScriptsExecutionItem.execute(
                    executionContainer.getExecutableContext(), executionContainer.getTasks(), groovyRunner, predicate);
            log.info("End process feedback. Context = {}.", executionContainer.getExecutableContext());
        } catch (Exception ex) {
            log.error("Error during process feedback.", ex);
        }
    }

    private void setStorageToScript(Class<?> groovyClass) {
        setServiceToScript(groovyClass, InjectProcessStorage.class, CompletedProcessStorage.class, completedProcessStorage);
    }
}
