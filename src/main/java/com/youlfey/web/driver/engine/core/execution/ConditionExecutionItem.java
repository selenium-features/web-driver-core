package com.youlfey.web.driver.engine.core.execution;

import com.youlfey.web.driver.engine.core.annotation.InjectContextManager;
import com.youlfey.web.driver.engine.core.annotation.InjectProcessStorage;
import com.youlfey.web.driver.engine.core.annotation.InjectWebDriver;
import com.youlfey.web.driver.engine.core.annotation.InjectWindowsHandler;
import com.youlfey.web.driver.engine.core.context.ContextManager;
import com.youlfey.web.driver.engine.core.drivers.WindowsHandler;
import com.youlfey.web.driver.engine.core.entity.GroovyTask;
import com.youlfey.web.driver.engine.core.entity.IndexedContainer;
import com.youlfey.web.driver.engine.core.groovy.GroovyExecutor;
import com.youlfey.web.driver.engine.core.groovy.runners.GroovyRunnerInstance;
import com.youlfey.web.driver.engine.core.pojo.ConditionFeedback;
import com.youlfey.web.driver.engine.core.pojo.ExecutableContext;
import com.youlfey.web.driver.engine.core.pojo.ExecutionContainer;
import com.youlfey.web.driver.engine.core.services.CompletedProcessStorage;
import com.youlfey.web.driver.engine.core.services.impl.LibrariesContainer;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;

import java.util.Arrays;
import java.util.Optional;
import java.util.function.Predicate;

import static com.youlfey.web.driver.engine.core.execution.GroovyServicePropagator.setServiceToScript;

@Slf4j
public class ConditionExecutionItem<T> {

    private final Predicate<ExecutableContext> predicate = context -> {
        Boolean availableForExecution = context.isAvailableForExecution();
        return availableForExecution != null && !availableForExecution;
    };

    private final CompletedProcessStorage<T> completedProcessStorage;
    private final ContextManager<T> contextManager;
    private final WebDriver webDriver;
    private final WindowsHandler windowsHandler;
    private final GroovyRunnerInstance groovyRunner;

    public ConditionExecutionItem(CompletedProcessStorage<T> completedProcessStorage,
                                  ContextManager<T> contextManager,
                                  WebDriver webDriver,
                                  WindowsHandler windowsHandler,
                                  GroovyExecutor groovyExecutor,
                                  LibrariesContainer librariesContainer) {
        this.completedProcessStorage = completedProcessStorage;
        this.contextManager = contextManager;
        this.webDriver = webDriver;
        this.windowsHandler = windowsHandler;
        this.groovyRunner = new GroovyRunnerInstance(groovyExecutor, librariesContainer,
                Arrays.asList(
                        this::setContextManagerToScript,
                        this::setStorageToScript,
                        this::setWebDriverToScript,
                        this::setWindowsHandlerToScript
                )
        );
    }

    public ConditionFeedback check(ExecutionContainer<T> container) {
        ExecutableContext<T> executableContext = container.getExecutableContext();
        IndexedContainer<GroovyTask<T>> tasks = container.getTasks();
        try {
            if (tasks.isEmpty()) {
                log.info("Condition tasks is empty. Return is not available for execution.");
                return new ConditionFeedback(false);
            }
            log.info("Try to resolve condition. Context = {}.", executableContext);

            ScriptsExecutionItem.execute(executableContext, tasks, groovyRunner, predicate);

            log.info("Condition is completed. Context = {}.", executableContext);

            return new ConditionFeedback(Optional.ofNullable(executableContext.isAvailableForExecution()).orElse(false));
        } catch (Exception ex) {
            log.error("Error during perform condition.", ex);
            return new ConditionFeedback(false);
        }
    }

    private void setWindowsHandlerToScript(Class<?> groovyClass) {
        setServiceToScript(groovyClass, InjectWindowsHandler.class, WindowsHandler.class, windowsHandler);
    }

    private void setWebDriverToScript(Class<?> groovyClass) {
        setServiceToScript(groovyClass, InjectWebDriver.class, WebDriver.class, webDriver);
    }

    private void setContextManagerToScript(Class<?> groovyClass) {
        setServiceToScript(groovyClass, InjectContextManager.class, ContextManager.class, contextManager);
    }

    private void setStorageToScript(Class<?> groovyClass) {
        setServiceToScript(groovyClass, InjectProcessStorage.class, CompletedProcessStorage.class, completedProcessStorage);
    }
}
