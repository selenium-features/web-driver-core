package com.youlfey.web.driver.engine.core.pojo;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.youlfey.web.driver.engine.core.entity.ProcessInformation;
import com.youlfey.web.driver.engine.core.entity.ScopeInformation;
import lombok.Getter;
import lombok.SneakyThrows;
import org.apache.commons.collections4.CollectionUtils;

import java.util.*;

public class ExecutableContext<T> extends HashMap<String, Object> {
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);
    private static final String FEEDBACK = "feedback";
    private static final String STATUS = "status";
    private static final String SYSTEM_PARAMETERS = "systemParameters";
    private static final String FAILED_PROCESSES = "failedProcesses";
    private static final String SUCCESS_PROCESSES = "successProcesses";
    private static final String IS_AVAILABLE_FOR_EXECUTION = "isAvailableForExecution";

    public ExecutableContext(ProcessInformation<T> processInformation,
                             ScopeInformation<T> scopeInformation,
                             Map<String, Object> context) {
        super();
        // To prevent NPE
        if (Objects.nonNull(context)) {
            this.putAll(context);
        }

        final SystemParameters<T> systemParameters = new SystemParameters<>(processInformation, scopeInformation);

        // Put to context to have access in the script
        this.put(SYSTEM_PARAMETERS, systemParameters);
        this.put(FAILED_PROCESSES, new HashSet<>());
        this.put(SUCCESS_PROCESSES, new HashSet<>());
    }

    public ExecutableContext(ProcessInformation<T> processInformation,
                             ScopeInformation<T> scopeInformation) {
        this(processInformation, scopeInformation, new HashMap<>());
    }

    public <NewType> NewType get(String key, Class<NewType> newTypeClass) {
        try {
            return newTypeClass.cast(get(key));
        } catch (ClassCastException | NullPointerException ex) {
            return getWithDeserialization(key, newTypeClass);
        }
    }

    private <NewType> NewType getWithDeserialization(String key, Class<NewType> newTypeClass) {
        try {
            return OBJECT_MAPPER.convertValue(get(key), newTypeClass);
        } catch (Exception ex) {
            return null;
        }
    }

    public void clearStatus() {
        remove(STATUS);
    }

    public void putStatus(String status) {
        put(STATUS, status);
    }

    public String getStatus() {
        return get(STATUS, String.class);
    }

    public Boolean isAvailableForExecution() {
        Object value = get(IS_AVAILABLE_FOR_EXECUTION);
        if (Objects.isNull(value)) {
            return null;
        }

        try {
            return Boolean.parseBoolean(String.valueOf(value));
        } catch (Throwable ex) {
            return false;
        }
    }

    public SystemParameters<T> getSystemParameters() {
        return get(SYSTEM_PARAMETERS, SystemParameters.class);
    }

    public Set<T> getSuccessProcesses() {
        return (Set<T>) get(SUCCESS_PROCESSES, Set.class);
    }

    public Set<T> getFailedProcesses() {
        return (Set<T>) get(FAILED_PROCESSES, Set.class);
    }

    public void putFailedProcess(T processId) {
        Set<T> failedProcesses = (Set<T>) get(FAILED_PROCESSES, Set.class);
        if (CollectionUtils.isEmpty(failedProcesses)) {
            failedProcesses = new HashSet<>();
            put(FAILED_PROCESSES, failedProcesses);
        }
        failedProcesses.add(processId);
    }

    public void putFailedProcesses(Set<T> processIds) {
        Set<T> failedProcesses = (Set<T>) get(FAILED_PROCESSES, Set.class);
        if (CollectionUtils.isEmpty(failedProcesses)) {
            failedProcesses = new HashSet<>();
            put(FAILED_PROCESSES, failedProcesses);
        }
        failedProcesses.addAll(processIds);
    }

    public void putSuccessProcess(T process) {
        Set<T> successProcesses = (Set<T>) get(SUCCESS_PROCESSES, Set.class);
        if (CollectionUtils.isEmpty(successProcesses)) {
            successProcesses = new HashSet<>();
            put(SUCCESS_PROCESSES, successProcesses);
        }
        successProcesses.add(process);
    }

    public void putSuccessProcesses(Set<T> processes) {
        Set<T> successProcesses = (Set<T>) get(SUCCESS_PROCESSES, Set.class);
        if (CollectionUtils.isEmpty(successProcesses)) {
            successProcesses = new HashSet<>();
            put(SUCCESS_PROCESSES, successProcesses);
        }
        successProcesses.addAll(processes);
    }

    public ProcessInformation<T> getProcessInformation() {
        final SystemParameters<T> systemParameters = getSystemParameters();
        return systemParameters.processInformation;
    }

    public Map<String, Object> getCleanContext() {
        final Map<String, Object> cleanContext = new HashMap<>(this);
        cleanContext.remove(STATUS);
        cleanContext.remove(FEEDBACK);
        cleanContext.remove(FAILED_PROCESSES);
        cleanContext.remove(SUCCESS_PROCESSES);
        cleanContext.remove(SYSTEM_PARAMETERS);
        return cleanContext;
    }

    @Override
    @SneakyThrows
    public String toString() {
        return OBJECT_MAPPER.writeValueAsString(this);
    }

    @Getter
    public static class SystemParameters<T> {
        private final ProcessInformation<T> processInformation;
        private final ScopeInformation<T> scopeInformation;

        public SystemParameters(ProcessInformation<T> processInformation,
                                ScopeInformation<T> scopeInformation) {
            // Ignore feedback script, tasks and condition
            this.processInformation = new ProcessInformation<>(
                    processInformation.getId(),
                    processInformation.getProcessName(),
                    null,
                    null,
                    null,
                    processInformation.getAdditionalParameters(),
                    processInformation.getOptions()
            );
            this.scopeInformation = new ScopeInformation<>(
                    scopeInformation.getId(),
                    scopeInformation.getName(),
                    scopeInformation.getProcesses(),
                    scopeInformation.getBrowserType(),
                    scopeInformation.getAdditionalParameters()
            );
        }
    }
}
