package com.youlfey.web.driver.engine.core.providers;

import com.youlfey.web.driver.engine.core.entity.GroovyTask;

import java.util.Map;

public interface TaskProvider<ID> {

    Map<Integer, GroovyTask<ID>> getTasks(Map<Integer, ID> taskIds);
}
