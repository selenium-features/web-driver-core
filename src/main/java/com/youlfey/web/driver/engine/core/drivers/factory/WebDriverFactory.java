package com.youlfey.web.driver.engine.core.drivers.factory;

import com.youlfey.web.driver.engine.core.enums.BrowserType;
import com.youlfey.web.driver.engine.core.exception.WebDriverDoesNotProvided;
import com.youlfey.web.driver.engine.core.providers.WebDriverProvider;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.openqa.selenium.WebDriver;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

import static java.util.stream.Collectors.toMap;

@Slf4j
public class WebDriverFactory {

    private final Map<BrowserType, WebDriverProvider> providers;

    public WebDriverFactory(List<WebDriverProvider> providers) {
        if (CollectionUtils.isEmpty(providers)) {
            log.info("There is not providers.");
            this.providers = Collections.emptyMap();
            return;
        }
        this.providers = providers.stream()
                .collect(toMap(WebDriverProvider::browserType, Function.identity()));
        log.info("Set providers for following browsers = {}.", this.providers.keySet());
    }

    public WebDriver getWebDriver(BrowserType browserType) {
        if (browserType == null) {
            log.info("Set {} as default browser.", BrowserType.CHROME);
            browserType = BrowserType.CHROME;
        }

        if (BrowserType.CHROME.equals(browserType)) {
            return getWebDriverInternal(browserType).orElseThrow(() -> new WebDriverDoesNotProvided(BrowserType.CHROME));
        }

        if (BrowserType.FIREFOX.equals(browserType)) {
            return getWebDriverInternal(browserType).orElseThrow(() -> new WebDriverDoesNotProvided(BrowserType.FIREFOX));
        }

        log.info("Browser {} not supported now.", browserType);

        throw new UnsupportedOperationException();
    }

    private Optional<WebDriver> getWebDriverInternal(BrowserType browserType) {
        log.info("Try to get web driver for browser {}.", browserType);
        return Optional.ofNullable(providers.get(browserType))
                .map(WebDriverProvider::get);
    }
}
