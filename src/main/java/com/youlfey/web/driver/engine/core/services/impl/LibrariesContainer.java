package com.youlfey.web.driver.engine.core.services.impl;

import com.youlfey.web.driver.engine.core.annotation.InjectLibrary;
import com.youlfey.web.driver.engine.core.services.LibrariesProvider;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Slf4j
public class LibrariesContainer implements LibrariesProvider {

    private final Map<String, Object> libraries = new HashMap<>();

    public LibrariesContainer registerLibrary(String libraryName, Object library) {
        log.info("Register library with type {} by name {}.", library.getClass(), libraryName);
        libraries.put(libraryName, library);
        return this;
    }

    private Object getLibrary(String libraryName) {
        log.info("Get library by name = {}.", libraryName);
        return libraries.get(libraryName);
    }

    @Override
    public void enrichGroovyClassWithLibraries(Class<?> groovyClass) {
        log.info("Enrich groovy class with libraries is started.");

        if (Objects.isNull(groovyClass)) {
            log.info("Groovy class is empty.");
            return;
        }

        final Field[] declaredFields = groovyClass.getDeclaredFields();

        for (Field declaredField : declaredFields) {
            final InjectLibrary annotation = declaredField.getAnnotation(InjectLibrary.class);
            if (Objects.isNull(annotation)) {
                log.info("Annotation is not presented for field {}. Move to next field.", declaredField.getName());
                continue;
            }

            String libraryName = annotation.name();

            if (libraryName.isEmpty()) {
                libraryName = declaredField.getType().getName();
            }

            log.info("Library name resolved = {}.", libraryName);

            try {
                Object library = getLibrary(libraryName);

                if (Objects.nonNull(library)) {
                    log.info("Library resolved for name {}. Try to set.", libraryName);
                    boolean isAccessible = declaredField.isAccessible();
                    try {
                        if (!isAccessible) {
                            declaredField.setAccessible(true);
                        }
                        declaredField.set(null, library);
                    } finally {
                        if (!isAccessible) {
                            declaredField.setAccessible(false);
                        }
                    }
                }
            } catch (Exception ex) {
                // just ignore
            }
        }

        log.info("Enrich groovy class with libraries is finished.");
    }
}
