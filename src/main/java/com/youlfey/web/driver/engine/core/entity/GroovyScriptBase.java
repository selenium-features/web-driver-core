package com.youlfey.web.driver.engine.core.entity;

import lombok.*;

import java.beans.ConstructorProperties;

@Getter
@ToString
@EqualsAndHashCode
@RequiredArgsConstructor(onConstructor_ = @ConstructorProperties({"groovyCode"}))
public class GroovyScriptBase {
    private final String groovyCode;

    // Only for dev
    @Setter
    private String groovyPathFile;
}
