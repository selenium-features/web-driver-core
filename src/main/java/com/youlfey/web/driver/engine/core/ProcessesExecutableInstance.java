package com.youlfey.web.driver.engine.core;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;

import java.util.Objects;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class ProcessesExecutableInstance {
    private final WebDriverEngine webDriverEngine;
    private final ScopeContainer<?> scopeContainer;

    public void executeAllAvailableProcesses() {
        webDriverEngine.executeAllAvailableProcesses(scopeContainer);
    }

    public ProcessesExecutableInstance registerLibrary(String libraryName, Object library) {
        webDriverEngine.registerLibrary(libraryName, library);
        return this;
    }

    public ProcessesExecutableInstance registerLibrary(Object library) {
        registerLibrary(library.getClass().getName(), library);
        return this;
    }

    public static ProcessesExecutableInstance newInstance(WebDriverEngine webDriverEngine, ScopeContainer<?> scopeContainer) {
        return new ProcessesExecutableInstance(
                Objects.requireNonNull(webDriverEngine),
                Objects.requireNonNull(scopeContainer)
        );
    }
}
