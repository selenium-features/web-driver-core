package com.youlfey.web.driver.engine.core.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.beans.ConstructorProperties;
import java.util.Map;

@Getter
@ToString
@EqualsAndHashCode
public class Scenario<ID> {
    private final IndexedContainer<ID> tasks;

    @ConstructorProperties({"tasks"})
    public Scenario(Map<Integer, ID> tasks) {
        this.tasks = new IndexedContainer<>(tasks);
    }
}
