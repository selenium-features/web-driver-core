package com.youlfey.web.driver.engine.core.dev;

public class DevMode {
    private static final String DEV_PROPERTY = "groovy.loader.dev";

    public static boolean isLoadGroovyClassFromFile() {
        return Boolean.getBoolean(DEV_PROPERTY);
    }
}
