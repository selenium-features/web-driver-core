package com.youlfey.web.driver.engine.core;

import com.youlfey.web.driver.engine.core.context.ContextManager;
import com.youlfey.web.driver.engine.core.context.SimpleContextManagerInMemory;
import com.youlfey.web.driver.engine.core.drivers.factory.WebDriverFactory;
import com.youlfey.web.driver.engine.core.entity.GroovyTask;
import com.youlfey.web.driver.engine.core.entity.IndexedContainer;
import com.youlfey.web.driver.engine.core.entity.ProcessInformation;
import com.youlfey.web.driver.engine.core.entity.ScopeInformation;
import com.youlfey.web.driver.engine.core.enums.BrowserType;
import com.youlfey.web.driver.engine.core.enums.WebDriverArrivalPlace;
import com.youlfey.web.driver.engine.core.exception.ScopeNotFound;
import com.youlfey.web.driver.engine.core.providers.ProcessProvider;
import com.youlfey.web.driver.engine.core.providers.ScopeProvider;
import com.youlfey.web.driver.engine.core.providers.TaskProvider;
import com.youlfey.web.driver.engine.core.services.CompletedProcessStorage;
import com.youlfey.web.driver.engine.core.services.impl.CompletedProcessStorageInMemory;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.openqa.selenium.WebDriver;

import java.util.*;

@Slf4j
public class ScopeContainer<ID> {

    @Getter
    private final ID scopeId;
    private final ScopeProvider<ID> scopeProvider;
    private final ProcessProvider<ID> processProvider;
    private final TaskProvider<ID> taskProvider;
    private final BrowserType browserType;
    private final WebDriverFactory webDriverFactory;
    @Getter
    private CompletedProcessStorage<ID> completedProcessStorage;
    @Getter
    private ContextManager<ID> contextManager;
    private WebDriverInstance webDriverInstance;

    private ScopeContainer(ID scopeId,
                           ScopeProvider<ID> scopeProvider,
                           ProcessProvider<ID> processProvider,
                           TaskProvider<ID> taskProvider,
                           WebDriverFactory webDriverFactory) {
        this.scopeId = scopeId;
        this.scopeProvider = scopeProvider;
        this.processProvider = processProvider;
        this.taskProvider = taskProvider;
        this.webDriverFactory = webDriverFactory;

        final Optional<ScopeInformation<ID>> scope = scopeProvider.getScope(scopeId);
        if (scope.isPresent()) {
            this.browserType = scope.get().getBrowserType();
            return;
        }
        throw new ScopeNotFound(scopeId);
    }

    public ScopeInformation<ID> getScopeInformation() {
        log.info("Try to get scope information by id = {}.", scopeId);
        return scopeProvider.getScope(scopeId).orElseThrow(() -> new ScopeNotFound(scopeId));
    }

    public List<ProcessInformation<ID>> getProcesses() {
        final ScopeInformation<ID> scopeInformation = getScopeInformation();
        final List<ID> processIds = scopeInformation.getProcesses();
        log.info("All processes for scope {} is {}.", processIds, scopeInformation);
        if (CollectionUtils.isEmpty(processIds)) {
            return Collections.emptyList();
        }
        log.info("Try to get processes by ids = {}.", processIds);
        return Optional.ofNullable(processProvider.getProcessInformation(processIds))
                .orElse(Collections.emptyList());
    }

    public IndexedContainer<GroovyTask<ID>> getExecutionTasks(ProcessInformation<ID> processInformation) {
        final IndexedContainer<ID> tasksIds = processInformation.getMainScenario().getTasks();
        log.info("Resolved all task ids {} for execution by process name {} and process id = {}.", tasksIds,
                processInformation.getProcessName(), processInformation.getId());
        return getAllTasksInternal(tasksIds);
    }

    public IndexedContainer<GroovyTask<ID>> getConditionTasks(ProcessInformation<ID> processInformation) {
        if (Objects.isNull(processInformation.getConditionScenario())) {
            return IndexedContainer.empty();
        }
        final IndexedContainer<ID> tasksIds = processInformation.getConditionScenario().getTasks();
        log.info("Resolved all task ids {} for condition by process name {} and process id = {}.", tasksIds,
                processInformation.getProcessName(), processInformation.getId());
        return getAllTasksInternal(tasksIds);
    }

    public IndexedContainer<GroovyTask<ID>> getFeedbackTasks(ProcessInformation<ID> processInformation) {
        if (Objects.isNull(processInformation.getFeedbackScenario())) {
            return IndexedContainer.empty();
        }
        final IndexedContainer<ID> tasksIds = processInformation.getFeedbackScenario().getTasks();
        log.info("Resolved all task ids {} for feedback by process name {} and process id = {}.", tasksIds,
                processInformation.getProcessName(), processInformation.getId());
        return getAllTasksInternal(tasksIds);
    }

    private IndexedContainer<GroovyTask<ID>> getAllTasksInternal(IndexedContainer<ID> tasksIds) {
        if (IndexedContainer.isEmpty(tasksIds)) {
            return IndexedContainer.empty();
        }

        log.info("Try to get tasks by ids {}.", tasksIds);
        final Map<Integer, GroovyTask<ID>> tasks = taskProvider.getTasks(tasksIds.getContainer());
        log.info("Found tasks {}.", tasks);
        return new IndexedContainer<>(tasks);
    }

    @Builder(builderMethodName = "webDriverProvider", buildMethodName = "provide")
    public WebDriverInstance getWebDriver(BrowserType browserType, WebDriverArrivalPlace place) {
        if (WebDriverArrivalPlace.REQUIRED_NEW.equals(place)) {
            log.info("Provider type is {}. Need to create new instance of web driver.", WebDriverArrivalPlace.REQUIRED_NEW);
            return new WebDriverInstance(webDriverFactory.getWebDriver(browserType));
        }
        log.info("Provider type is {}. Use exists web driver for scope.", place);
        if (Objects.isNull(webDriverInstance)) {
            synchronized (this.browserType) {
                if (Objects.isNull(webDriverInstance)) {
                    webDriverInstance = new WebDriverInstance(webDriverFactory.getWebDriver(this.browserType));
                }
            }
        }
        return webDriverInstance;
    }

    public static <T> ScopeContainer<T> newInstance(T scopeId,
                                                    ScopeProvider<T> scopeProvider,
                                                    ProcessProvider<T> processProvider,
                                                    TaskProvider<T> taskProvider,
                                                    WebDriverFactory factory) {
        log.info("Create new instance of ScopeContainer. Create context manager and completed process storage in memory by default.");
        return newInstance(scopeId, scopeProvider, processProvider, taskProvider, factory,
                new CompletedProcessStorageInMemory<>(),
                new SimpleContextManagerInMemory<>());
    }

    public static <T> ScopeContainer<T> newInstance(T scopeId,
                                                    ScopeProvider<T> scopeProvider,
                                                    ProcessProvider<T> processProvider,
                                                    TaskProvider<T> taskProvider,
                                                    WebDriverFactory factory,
                                                    CompletedProcessStorage<T> storage) {
        log.info("Create new instance of ScopeContainer. Create context manager in memory by default.");
        return newInstance(scopeId, scopeProvider, processProvider, taskProvider, factory,
                storage,
                new SimpleContextManagerInMemory<>());
    }

    public static <T> ScopeContainer<T> newInstance(T scopeId,
                                                    ScopeProvider<T> scopeProvider,
                                                    ProcessProvider<T> processProvider,
                                                    TaskProvider<T> taskProvider,
                                                    WebDriverFactory factory,
                                                    ContextManager<T> contextManager) {
        log.info("Create new instance of ScopeContainer. Create completed process storage in memory by default.");
        return newInstance(scopeId, scopeProvider, processProvider, taskProvider, factory,
                new CompletedProcessStorageInMemory<>(),
                contextManager);
    }

    public static <T> ScopeContainer<T> newInstance(T scopeId,
                                                    ScopeProvider<T> scopeProvider,
                                                    ProcessProvider<T> processProvider,
                                                    TaskProvider<T> taskProvider,
                                                    WebDriverFactory factory,
                                                    CompletedProcessStorage<T> completedProcessStorage,
                                                    ContextManager<T> contextManager) {
        log.info("Create new instance of ScopeContainer.");
        final ScopeContainer<T> container = new ScopeContainer<>(
                Objects.requireNonNull(scopeId),
                Objects.requireNonNull(scopeProvider),
                Objects.requireNonNull(processProvider),
                Objects.requireNonNull(taskProvider),
                Objects.requireNonNull(factory)
        );
        container.completedProcessStorage = Objects.requireNonNull(completedProcessStorage);
        container.contextManager = Objects.requireNonNull(contextManager);
        return container;
    }

    @Getter
    public static final class WebDriverInstance {
        private final WebDriver webDriver;
        private final String mainWindowId;

        public WebDriverInstance(WebDriver webDriver) {
            this.webDriver = webDriver;
            this.mainWindowId = webDriver.getWindowHandle();
        }
    }
}
