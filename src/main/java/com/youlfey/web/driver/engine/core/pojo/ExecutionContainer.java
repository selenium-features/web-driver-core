package com.youlfey.web.driver.engine.core.pojo;

import com.youlfey.web.driver.engine.core.entity.GroovyTask;
import com.youlfey.web.driver.engine.core.entity.IndexedContainer;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@Getter
@EqualsAndHashCode
@ToString
@RequiredArgsConstructor
public class ExecutionContainer<ID> {
    private final ExecutableContext<ID> executableContext;
    private final IndexedContainer<GroovyTask<ID>> tasks;
}
