package com.youlfey.web.driver.engine.core.context;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;

import java.util.function.Consumer;

@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
public final class ContextCloser<T> implements AutoCloseable {

    private final T context;
    private final Consumer<T> closer;

    @Override
    public void close() {
        closer.accept(context);
    }
}
