package com.youlfey.web.driver.engine.core.exception;

public class GroovyCodeShouldBeProvided extends RuntimeException {
    public GroovyCodeShouldBeProvided(String taskName) {
        super("Groovy code is empty for task " + taskName);
    }
}
