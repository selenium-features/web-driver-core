package com.youlfey.web.driver.engine.core.services;

public interface LibrariesProvider {

    void enrichGroovyClassWithLibraries(Class<?> groovyClass);
}
