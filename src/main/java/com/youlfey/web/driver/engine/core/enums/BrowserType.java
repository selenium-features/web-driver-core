package com.youlfey.web.driver.engine.core.enums;

public enum BrowserType {
    CHROME, FIREFOX
}
