package com.youlfey.web.driver.engine.core.context;

public interface ContextManager<T> {

    ContextCloser<?> context(ContextObject<T> context);

    boolean contains(Object key);

    int size();
}
