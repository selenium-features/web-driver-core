package com.youlfey.web.driver.engine.core.context;

import lombok.extern.slf4j.Slf4j;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Slf4j
public class SimpleContextManagerInMemory<T> implements ContextManager<T> {
    private final Map<T, ContextObject<T>> mapProcessIdToContext = Collections.synchronizedMap(new HashMap<>());

    @Override
    public ContextCloser<?> context(ContextObject<T> context) {
        log.info("Create context with data = {}.", context);
        T id = context.getProcessInformation().getId();
        mapProcessIdToContext.put(id, context);
        return new ContextCloser<>(id, mapProcessIdToContext::remove);
    }

    @Override
    public boolean contains(Object key) {
        return mapProcessIdToContext.containsKey(key) || mapProcessIdToContext.containsValue(key);
    }

    @Override
    public int size() {
        return mapProcessIdToContext.size();
    }
}
