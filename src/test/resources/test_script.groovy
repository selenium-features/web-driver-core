import com.youlfey.web.driver.engine.core.annotation.InjectContextManager
import com.youlfey.web.driver.engine.core.annotation.InjectLibrary
import com.youlfey.web.driver.engine.core.annotation.InjectProcessStorage
import com.youlfey.web.driver.engine.core.annotation.InjectWebDriver
import com.youlfey.web.driver.engine.core.context.SimpleContextManagerInMemory
import com.youlfey.web.driver.engine.core.pojo.ExecutableContext
import com.youlfey.web.driver.engine.core.services.CompletedProcessStorage
import groovy.transform.Field
import org.openqa.selenium.WebDriver

import static groovy.test.GroovyTestCase.assertNotNull

@Field
@InjectWebDriver
private static WebDriver webDriver

@Field
@InjectProcessStorage
private static CompletedProcessStorage completedProcessStorage

@Field
@InjectContextManager
private static SimpleContextManagerInMemory contextManager

@Field
@InjectLibrary(name = "TestLibrary")
private static Object testLibrary

def execute(ExecutableContext context) {
    assertNotNull(webDriver)
    assertNotNull(completedProcessStorage)
    assertNotNull(contextManager)
    assertNotNull(testLibrary)

    context.status = "OK"
}
